import React from 'react';
import ProductCard from "./ProductCard/ProductCard";

const Store = ({products}) => {
    return (
        <div className="Store">
            <h2 className="mt-5 my-3">Магазин</h2>
            <div className="row">
                {products.map(item => {
                    return <div className="col-12 col-md-4" key={Math.random()} >
                        <ProductCard name={item.name} image={item.image} text={item.text} />
                    </div>
                })}
            </div>
        </div>
    );
};

export default Store;