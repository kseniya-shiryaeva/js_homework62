import React from 'react';

const ProductCard = ({name, image, text}) => {
    return (
        <div className="ProductCard card">
            <img src={image} className="card-img-top" alt="product1" />
            <div className="card-body">
                <h5 className="card-title">{name}</h5>
                <p className="card-text">{text}</p>
            </div>
        </div>
    );
};

export default ProductCard;