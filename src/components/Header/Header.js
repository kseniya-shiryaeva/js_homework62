import React from 'react';
import './Header.css';
import Logo from '../../assets/logo.jpg';

const Header = () => {
    return (
        <header className="Header">
            <div className="container">
                <div className="d-flex justify-content-between">
                    <a href="/"><img src={Logo} alt="logo"/></a>
                    <a href="tel:+996 555 159 955" className="align-self-center">+996 555 159 955</a>
                </div>
            </div>
        </header>
    );
};

export default Header;