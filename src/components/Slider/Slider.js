import React from 'react';
import './Slider.css';

const Slider = ({slides}) => {
    return (
        <div className="Slider">
            <div id="mainSliderIndicators" className="carousel slide" data-ride="carousel">
                <ol className="carousel-indicators">
                    {slides.map((item, key) => {
                        if(key === 0) {
                            return <li data-target="#mainSliderIndicators" data-slide-to={key} className="active" key={key}> </li>
                        }
                        return <li data-target="#mainSliderIndicators" data-slide-to={key} key={key}> </li>
                    })
                    }
                </ol>
                <div className="carousel-inner">
                    {slides.map((item, key) => {
                            if(key === 0) {
                                return <div className="carousel-item active" key={key}>
                                    <img src={item} className="d-block w-100" alt="slide1"/>
                                </div>
                            }
                            return <div className="carousel-item" key={key}>
                                <img src={item} className="d-block w-100" alt="slide1"/>
                            </div>
                        })
                    }
                </div>
                <a className="carousel-control-prev" href="#mainSliderIndicators" role="button" data-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span className="sr-only">Previous</span>
                </a>
                <a className="carousel-control-next" href="#mainSliderIndicators" role="button" data-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                    <span className="sr-only">Next</span>
                </a>
            </div>
        </div>
    );
};

export default Slider;