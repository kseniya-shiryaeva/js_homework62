import React from 'react';
import './MainMenu.css';
import {NavLink} from "react-router-dom";

const MainMenu = () => {
    return (
        <nav className="MainMenu">
            <div className="container">
                <NavLink to="/" exact>Главная</NavLink>
                <NavLink to="/about">О нас</NavLink>
                <NavLink to="/contacts">Контакты</NavLink>
            </div>
        </nav>
    );
};

export default MainMenu;