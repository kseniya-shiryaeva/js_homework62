import './App.css';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import Home from "./containers/Home/Home";
import AboutPage from "./containers/AboutPage/AboutPage";
import Contacts from "./containers/Contacts/Contacts";
import Header from "./components/Header/Header";
import MainMenu from "./components/MainMenu/MainMenu";
import Footer from "./components/Footer/Footer";

const App = () => (
    <>
        <Header />
        <BrowserRouter>
            <MainMenu />
            <div className="container">
                <Switch>
                    <Route path="/" exact strict component={Home} />
                    <Route path="/about" component={AboutPage} />
                    <Route path="/contacts" component={Contacts} />
                </Switch>
            </div>
        </BrowserRouter>
        <Footer />
    </>
);

export default App;
