import React from 'react';
import './Contacts.css';

const Contacts = () => {
    return (
        <div className="Contacts">
            <h1 className="my-3">Контакты</h1>
            <div className="content pt-4">
                <p><b>Адреса:</b>ЦУМ 10:00-20:00<br/>
                    Дом торговли 10:00-19:00</p>
                <p><b>Телефон:</b> <a href="tel:+996 555 159 955">+996 555 159 955</a></p>
                <p><b>Instagram:</b> <a href="https://www.instagram.com/muzstore_kg/">muzstore_kg</a></p>
            </div>
        </div>
    );
};

export default Contacts;