import React from 'react';
import './AboutPage.css';
import Piano from '../../assets/slide4.jpg';

const AboutPage = () => {
    return (
        <div className="AboutPage">
            <h1 className="my-3">О нас</h1>
            <div className="content">
                <div className="row">
                    <div className="col-6">
                        <img src={Piano} alt="piano" className="w-100 h-auto"/>
                    </div>
                    <div className="col-6">
                        <ul className="h-100">
                            <li>Продаем музыкальные инструменты</li>
                            <li>Делаем мелкий ремонт</li>
                            <li>Даем уроки игры на гитаре</li>
                            <li>Даем советы и развлекаем</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AboutPage;