import React, {useState} from 'react';
import './Home.css';
import Slider from "../../components/Slider/Slider";
import Store from "../../components/Store/Store";
import Slide1 from '../../assets/slide1.jpg';
import Slide2 from '../../assets/slide2.jpg';
import Slide3 from '../../assets/slide3.jpg';
import Product1 from '../../assets/product1.jpg';
import Product2 from '../../assets/product2.jpg';
import Product3 from '../../assets/product3.jpg';

const Home = () => {
    const [slides] = useState([Slide1, Slide2, Slide3]);
    const [products] = useState([
        {
            name: 'Фортепиано',
            image: Product1,
            text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam commodi corporis velit'
        },
        {
            name: 'Гитара',
            image: Product2,
            text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam commodi corporis velit'
        },
        {
            name: 'Скрипка',
            image: Product3,
            text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam commodi corporis velit'
        }
    ]);

    return (
        <div className="Home">
            <Slider slides={slides} />
            <Store products={products} />
        </div>
    );
};

export default Home;